// names in arr split by space
data.forEach((item) => {
    console.log(item.name.split(' '))
});

// people over 50
const peopleOver50 = data.filter((item) => {
    return item.age > 50
});
console.log(peopleOver50);

// avg age
const avgAgeAll = data.reduce(function (prevItem, currItem) {
    return prevItem + currItem.age;
}, 0) / data.length;
console.log(avgAgeAll);

// states
const states = (data) => {
    let address = [];
    data.forEach((item) => {
        let splitedItem = item.address.split(',');
        address.push(splitedItem.slice(2, 3));
    });
    return address;

};
console.log(states(data).join());

// count people with blue eyes

const filterByEyes = data.filter((item) => {
    return item.eyeColor === 'blue'
});
console.log(filterByEyes.length);

//count male
const filterByGender = data.filter((item) => {
    return item.gender === 'male'
});
console.log(filterByGender.length);

// avg friends age
const avgFriendsAge = data.reduce((prevItem, currItem) => {
    const avg = currItem.friends.reduce((prevItem, currItem) => {
        return prevItem + currItem.age;
    }, 0) / 10;
    return prevItem + avg;
}, 0) / data.length;
console.log(avgFriendsAge);